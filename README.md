# My Portfolio Page

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## Packages

- [nuxt](https://nuxtjs.org/)
- [@nuxt/content](https://content.nuxtjs.org/)
- [tailwind](https://tailwindcss.com/)
- [jest](https://jestjs.io/)