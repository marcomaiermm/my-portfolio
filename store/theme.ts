export const state = () => ({
  theme: null,
})

export const actions = {
  toggleTheme({ commit }: any) {
    switch (localStorage.theme) {
      case 'light':
        commit('setTheme', 'dark')
        break
      default:
        commit('setTheme', 'light')
        break
    }
  },
  initTheme({ commit }: any) {
    const cachedTheme = localStorage.theme ? localStorage.theme : false

    const userPrefersDark = window.matchMedia(
      '(prefers-color-scheme: dark)'
    ).matches

    if (cachedTheme) {
      commit('setTheme', cachedTheme)
    } else if (userPrefersDark) {
      commit('setTheme', 'dark')
    } else {
      commit('setTheme', 'light')
    }
  },
}

export const mutations = {
  setTheme(state: any, theme: string) {
    state.theme = theme
    localStorage.theme = theme
  },
}

export const getters = {
  getTheme: (state: any) => state.theme,
}
