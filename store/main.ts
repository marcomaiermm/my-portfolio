export const state = () => {
    return {
        more: false
    }
}

export const actions = {
    toggleMore({ commit }: any) {
        commit('toggleMore')
    }
}

export const mutations = {
    toggleMore(state: any) {
        state.more = !state.more
    }
}