---
title: Introduction
description: An introduction of myself to the world ✨
image: /Me.jpg
---

# Hi, it's me 👋

My name is Marco and I'm from Germany, more precisely from near the beautiful Lake Constance.

However, I currently live in Vienna to study Industrial Engineering - Mechanical Engineering at the Technical University of Vienna.

I'm a self-taught full-stack developer with some experience in DevOps.
