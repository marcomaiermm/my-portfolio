---
title: Times
description: Each year 1400 IEM students in 350 teams compete in one of Europe's biggest case study competition for the title of "IEM students of the year".
sort: 1
image: /Times.jpg
stack: ['alpinejs', 'typescript', 'tailwindcss', 'vite']
url: https://relaxed-morse-2d1115.netlify.app/
more: false
---

## TIMES

TIMES is an event where every year about 1400 students in 350 teams compete in an international case study competition.

This project is intended to be a info page for the event and to provide information to participating students, organizers, judges and interested parties. A list of participants, as well as jurors and an event timetable are available for everyone to see at a glance.
