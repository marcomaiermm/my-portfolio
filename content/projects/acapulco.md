---
title: Acapulco Jumper
description: Simulation of an  Acapulco Jumper into the water
sort: 2
image: /AcapulcoJumper.jpg
stack: ['fastapi', 'nuxtjs', 'tailwindcss']
url: https://elated-goodall-1d4399.netlify.app/
slug: acapulco
more: true
---

## Acapulco Jumper

This is a study project at the Vienna University of Technology.

The task was to use Matlab to continuously simulate a jump of an Acapulco jumper from variable height and take-off speed in a mechanically correct way.
In addition, air resistance and weight were to be taken into account, as well as a fully stretched final position of the jumper.

The project is rewritten in Python and uses the numpy and sympy libraries for the calculation. The backend is Fastapi, the frontend is a Nuxt app.
