---
title: Allweier Homepage
description: Allweier Homepage made by me.
sort: 3
image: /AllweierHomepage.jpg
stack: ['django', 'wagtail', 'alpinejs', 'tailwindcss']
url: https://allweier-home.xyz/
more: false
---

## Allweier Präzisionsteile GmbH Homepage

I have created a homepage for the company Allweier Präzisionsteile GmbH and Allweier Systeme GmbH.

This uses Django with the Wagtail CMS and uses PostgreSQL as database. I sprinkled in some AlpineJS to add some simple user interactivity.
