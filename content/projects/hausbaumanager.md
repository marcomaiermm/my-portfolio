---
title: Hausbaumanager.app
description: Hausbaumanager.app is a saas for guiding and helping with planning for a new house.
sort: 0
image: /Hausbaumanager.app.jpg
stack: ['django', 'wagtail', 'alpinejs', 'tailwindcss']
url: https://hausbaumanager.app/
slug: hausbaumanager
more: false
---

## Hausbaumanager.app

The website of the hausbaumanager.app. This site was created for a young startup from Vienna, who want to streamline the communication and planning process for private builders.